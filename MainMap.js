require([
  "esri/map",
  "esri/tasks/QueryTask",
  "esri/toolbars/edit",
  "esri/graphic",
  "esri/lang",
  "esri/tasks/RelationshipQuery",
  "esri/layers/FeatureLayer",
  "esri/symbols/PictureMarkerSymbol",
  "esri/symbols/SimpleFillSymbol",
   "esri/symbols/SimpleLineSymbol",
  "esri/dijit/editing/Editor",
  "esri/dijit/editing/TemplatePicker",
  "esri/renderers/SimpleRenderer",
  "esri/renderers/ClassBreaksRenderer",
  "esri/config",
  "esri/request",
  "dojo/_base/array", 
  "esri/Color",
  "dojo/parser", 
  'dojo/_base/lang',
  "esri/tasks/query",
  "esri/InfoTemplate",
  "dojo/dom",
  "dojo/on",
  "esri/dijit/Search",
  "esri/geometry/Point",
  "esri/layers/GraphicsLayer",
  "esri/dijit/BasemapToggle",
  "dijit/TooltipDialog",
  "dijit/popup",
  "dojo/dom-style",
  "dojo/number",
  "esri/dijit/HomeButton",
  "dojo/domReady!"
], function(
  Map, QueryTask, Edit, Graphic, esriLang, RelationshipQuery,
  FeatureLayer,
  PictureMarkerSymbol, SimpleFillSymbol, SimpleLineSymbol,
  Editor, TemplatePicker, SimpleRenderer,ClassBreaksRenderer,
  esriConfig, esriRequest,
  arrayUtils, Color, parser, lang, Query, InfoTemplate, dom, on,
  Search, Point, GraphicsLayer, BasemapToggle, TooltipDialog, dijitPopup, domStyle, number, HomeButton
) {

    esri.arcgis.utils.arcgisUrl = "http://stockland.maps.arcgis.com/sharing/rest/content/items";

    var lods =
            [
                 {
                     "level": 1,
                     "resolution": 1.058335,
                     "scale": 4000
                 },
                {
                    "level": 2,
                    "resolution": 0.714376,
                    "scale": 2700
                },
                 {
                     "level": 3,
                     "resolution": 0.529168,
                     "scale": 2000
                 },
                 {
                     "level": 4,
                     "resolution": 0.396876,
                     "scale": 1500
                 },
                {
                    "level": 5,
                    "resolution": 0.317301,
                    "scale": 1200
                }, {
                    "level": 6,
                    "resolution": 0.238125,
                    "scale": 900
                },{
                     "level": 7,
                     "resolution": 0.185209,
                     "scale": 700
                 },{
                     "level": 8,
                     "resolution": 0.132292,
                     "scale": 500
                 },{
                     "level": 9,
                     "resolution": 0.092604,
                     "scale": 350
                 }, {
                     "level": 10,
                     "resolution": 0.066146,
                     "scale": 250
                 }, {
                     "level": 11,
                     "resolution": 0.052917,
                     "scale": 200
                 }];



    //var mapid = "4e1baab3ffbb4dadb06f30ee1a7ba00d";
    var mapid = "313e1aa7cf884ad1bfc54974186da3c7";
    var deferred = esri.arcgis.utils.createMap(mapid, "map", {
        mapOptions: {
            //backgroundColor: '#dbdddc',
            slider: true, 
            sliderPosition: "bottom-right",
            logo:false,
            lods: lods
            //maxZoom: 10,
            //minZoom: 1,
            //maxScale:2700
        }
    });
    var map;
    var bookmarks;
    var basemap;
    var mapLayersDic = {};
    var floorNumbers;
    var initialExtent;

    //Constants----------------------
    var storeLayerName = "StocklandRetailCentres_WFL1 - StocklandStores";    
    var searchFiledName = "Tenant"; //This field is used in search widget as store name
    var levelFieldName = "Level_"; 
    var levelSortFieldName = "LevelSort";
    var storeIdFieldName = "SAPID" //This field is used for zoom to a user selected store in store detail map page
    var centreIdFieldName = "AssetID";
    //-------------------------------

    var currentVisibleLevel = "G";
    deferred.then(function (response) {
        map = response.map;

        //This shows the popup window on top of the selected feature
        map.infoWindow.set("anchor", "top");
        //map.infoWindow.set("border", "2px solid #fff;");

        bookmarks = response.itemInfo.itemData.bookmarks;                
       // var layers = response.itemInfo.itemData.operationalLayers;

        getMapLayers();       

        var StoreSapId = "";
        //  var CentreSapiId = "3105201";
       //var CentreSapiId = "3107801";
        //var CentreSapiId = "3102601";
        //var CentreSapiId = "3103001";
        if (typeof CentreSapiId !== 'undefined')
            ZoomToBookmark(CentreSapiId);
            //zoomToAssetBase(CentreSapiId);
        else {
            initialExtent = map.extent;
            console.log("Error zoom to bookmark: CentreSapiId is undefined.")
        }
        
        var floorNumbers = getFloorNumbers();

        addHighlightingEffect();

        var itIsFirstTime = true;
        map.on('extent-change', function (event) {

            if (itIsFirstTime) {
                //As I have set custom zoom levels the initialExtent dose not set exactly to map extent. so after map.extent is set based on LODs and the 
                //bookmark extent(don't know how exactly) I again set the initilExtent with current map extent
                initialExtent = map.extent;
                itIsFirstTime = false;
            }

            var extent = map.extent.getCenter();
            if (initialExtent.contains(extent)) { }
            else { map.setExtent(initialExtent) }
        });

       
        map.graphics.enableMouseEvents();
        //map.graphics.on("mouse-out", closeDialog);
       // map.infoWindow.set("fillSymbol", new SimpleFillSymbol().setColor(new Color([170, 190, 255, 0.35])));

        //var homeButton = new HomeButton({
        //    theme: "HomeButton",
        //    map: map,
        //    extent: null,
        //    visible: true
        //}, dojo.query(".esriSimpleSliderDecrementButton")[0], "after");
        //homeButton.startup();


        addHomeSlider();

        //Search widget
        var search = new Search({
            map: map,
            allPlaceholder: "Search stores"
        }, "search");

        search.on("load", function () {
            var sources = search.sources;//[];//search.get("sources");
            //sources.clear();
            sources.push({
                featureLayer: map.getLayer(mapLayersDic[storeLayerName + "_Search"]),
                searchFields: [searchFiledName],
                displayField: searchFiledName,
                placeholder: "Search stores",
                searchExtent: initialExtent,
                exactMatch: false,
                outFields: [searchFiledName, "SAPID", levelFieldName],
                name: "Store Name",
                //highlightSymbol: symbol,
                maxResults: 6,
                maxSuggestions: 6,
                zoomScale: 6000,
                enableSuggestions: true,
                minCharacters: 0
            });
            sources.splice(0, 1); //Remove the geocoder source            
            search.set("sources", sources);            
            search.activeSource = search.sources[0];
            search.on("select-result", showSearchLocation);            
        });
        search.startup();

        //esriRequest.setRequestPreCallback(myCallbackFunction);
        //function myCallbackFunction(args) {
        //    //if (args.content.where && args.content.where.indexOf(searchFiledName) > -1) //Search is supposed to b on all levels, so I need to remove the definition query on Level_. 
        //    if (args.content.where)
        //        if (args.content.where.indexOf(searchFiledName) > -1) //Search is supposed to b on all levels, so I need to remove the definition query on Level_. 
        //    {
        //        args.content.where = args.content.where.split("AND")[1];
        //        args.content.where = args.content.where + " AND (AssetID = '" + CentreSapiId + "')";
        //    }

        //    return args;
        //}


    }, function (error) {
        console.log("Error: ", error.code, " Message: ", error.message);
        deferred.cancel();
    });

    function addHomeSlider() {

        //let's add the home button slider as a created class, requrires dom-Attr  
        dojo.create("div", {
            className: "esriSimpleSliderHomeButton",
            title: 'Zoom to Full Extent',
            onclick: function () {
                if (initialExtent === undefined) {
                    initialExtent = map.extent;
                }
                map.setExtent(initialExtent);
            }
        }, dojo.query(".esriSimpleSliderIncrementButton")[0], "after");
    }

    function getMapLayers() {  
        
        for (var j = 0, jl = map.graphicsLayerIds.length; j < jl; j++) {
            if (map.graphicsLayerIds[j] != "labels") {
                var currentLayer = map.getLayer(map.graphicsLayerIds[j]);                
                mapLayersDic[currentLayer.arcgisProps.title] = currentLayer.id;
                //layerNames.push(currentLayer.arcgisProps.title);
            }                      
        }  
    }
    
    function ZoomToBookmark(bookmarkName)
    {
        for (var i = 0, il = bookmarks.length; i < il; i++) {
            if (bookmarks[i].name == bookmarkName) {
                initialExtent = new esri.geometry.Extent(bookmarks[i].extent);
                map.setExtent(initialExtent);
               
                return;
            }
        }
        //The CentreSpaId is not a valid one. None of the bookmarks equal to it.
        initialExtent = map.extent;
        console.log("Error zoom to bookmark: CentreSapiId = " + bookmarkName + " is invalid.");
    }
    function zoomToAssetBase(CentreSapiId)
    {
        var assetLayer = map.getLayer(mapLayersDic["StocklandRetailCentres_WFL1 - StocklandAssetBase"]);
        var query = new Query();
        //query.where = "";  // query for non-null values  
        query.returnGeometry = true;          // turn geometry off, required to be false when using returnDistinctValues  
        query.where = "AssetID = '" + CentreSapiId  + "'";

        var queryTask = new QueryTask(assetLayer.url);
        queryTask.execute(query, function (featureset) {
            asset = featureset.features[0];
            initialExtent = asset.geometry.getExtent().expand(1.2)
            map.setExtent(initialExtent);
        });
    }

    function getFloorNumbers() {
        var storeLayer = map.getLayer(mapLayersDic[storeLayerName]);
        var query = new Query();
        //query.where = "";  // query for non-null values  
        query.orderByFields = [levelSortFieldName];    // sort it so we won't have to later  
        query.returnGeometry = false;          // turn geometry off, required to be false when using returnDistinctValues  
        query.returnDistinctValues = true;
        query.outFields = [levelFieldName, levelSortFieldName];
        query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_CONTAINS;
        query.geometry = initialExtent;//using map.extent caused problem. Seems the initialExtent has not been set to map yet

        var queryTask = new QueryTask(storeLayer.url);
        queryTask.execute(query, function (featureset) {
            floorNumbers = featureset.features.length;

            if (floorNumbers == 1) return;

            var zoomSlider = document.getElementById("map_zoom_slider");

            for (var i = 0 ; i < floorNumbers; i++) {
                var level = featureset.features[i].attributes[levelFieldName];
                var levelSort = featureset.features[i].attributes[levelSortFieldName];

                var lbl = document.createElement("label");
                lbl.innerHTML = level;
                lbl.setAttribute("Class", "input-check");

                if (level == "G")
                    lbl.classList.add("checked");

                var input = document.createElement("input");
                input.setAttribute("Type", "checkbox");
                input.setAttribute("ID", level);
                //input.setAttribute("onchange", "changeLavelsState(this)");
                input.onchange = changeLavelsState;

                //zoomSlider.appendChild(lbl);

                zoomSlider.insertBefore(lbl, zoomSlider.childNodes[0]);

                lbl.appendChild(input);

                var br = document.createElement("br");
            }
        });
    }

    function changeLavelsState(obj) {
        if (obj.target)
            obj = obj.target;

        var storeLayer = map.getLayer(mapLayersDic[storeLayerName]);

        if (obj.checked) {
            //Change the state of current visible lavel to unchecked
            if (obj.getAttribute("ID") != currentVisibleLevel) {
                document.getElementById(currentVisibleLevel).checked = false;
                changeLavelsState(document.getElementById(currentVisibleLevel));
                currentVisibleLevel = obj.getAttribute("ID");
            }

            //if checkbox is being checked, add a "checked" class
            obj.parentNode.classList.add("checked");

            //trun the level on
            storeLayer.setDefinitionExpression(levelFieldName + " = '" + obj.getAttribute("ID") + "'");            
        }
        else {
            //else remove it
            obj.parentNode.classList.remove("checked");
            //turn the level off
        }
    }

    function showSearchLocation(evt) {
        map.graphics.clear();

        //Find this store level button element, trun it on and change the button state to checked
        if (document.getElementById(evt.result.feature.attributes[levelFieldName])) {
            levelButton = document.getElementById(evt.result.feature.attributes[levelFieldName]);
            levelButton.checked = true;
            changeLavelsState(levelButton);
        }
        var sapid = evt.result.feature.attributes["SAPID"];
        var centerPoint = evt.result.extent.getCenter();
        var loc = map.toScreen(centerPoint);        
        map.infoWindow.set("fillSymbol", null);
        map.infoWindow.set("anchor", "top");

        var highlightSymbol = new SimpleFillSymbol(
            SimpleFillSymbol.STYLE_SOLID,
            null,
            new Color([170, 190, 255, 0.35])
       );

        getStoreDetails(sapid,
               function (response) {
                   if (response == "") //No responce from the callout
                       response = '{"StoreName": "' + evt.result.feature.attributes[searchFiledName] + '" }';
                   var content = createPopupContent(response);
                   map.infoWindow.setTitle(" ");
                   map.infoWindow.setContent(content);                  
                   map.infoWindow.show(loc);                   

                   var highlightGraphic = new Graphic(evt.result.feature.geometry, highlightSymbol);
                   map.graphics.add(highlightGraphic);

               });
    }

    function addHighlightingEffect()
    {
        var storeLayer = map.getLayer(mapLayersDic[storeLayerName]);

        var highlightSymbol = new SimpleFillSymbol(
         SimpleFillSymbol.STYLE_SOLID,
         null,
         new Color([170, 190, 255, 0.35])
        );

        var highlightedStore;
        storeLayer.on("mouse-over", function (evt) {
            map.graphics.clear();
            map.setMapCursor("pointer");
            var highlightGraphic = new Graphic(evt.graphic.geometry, highlightSymbol);
            map.graphics.add(highlightGraphic);
            highlightedStore = evt.graphic;
        });

        map.graphics.on("mouse-out", function (evt) {
            map.graphics.clear();
            map.setMapCursor("default");
        });

        map.graphics.on("click", function (evt) {
            if (!highlightedStore)
                return;
            getStoreDetails(highlightedStore.attributes["SAPID"],
               function (response) {
                   if (response == "") //No responce from the callout
                       response = '{"StoreName": "' + highlightedStore.attributes[searchFiledName] + '" }';

                   var infoTemplate = new InfoTemplate({ anchor: "top" });
                   infoTemplate.setTitle(" ");
                   infoTemplate.setContent(createPopupContent(response));
                   evt.graphic.setInfoTemplate(infoTemplate);
               });
        });       
    }

    function createPopupContent(response)
    {                
        var storeInfoArr = JSON.parse(response);
        var content;
        if (Object.keys(storeInfoArr).length == 1) {
            //Showing just the store name
            var storename = storeInfoArr.StoreName;
            content = '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '</div>';
            return content;
        }

        var storename = storeInfoArr.StoreName;
        var storeUrl = storeInfoArr.StorePageUrl;
        var storeLogo = storeInfoArr.StoreLogoUrl;
        var openingHours = storeInfoArr.OpeningHours;
        var phoneNumber = storeInfoArr.PhoneNumber;
        var level = storeInfoArr.Level;
        var storePageUrl = storeInfoArr.StorePageUrl;

        //Create popup html content
        content = "<a  href =" + "'" + storePageUrl + "'" + ">" +
            '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '<div class="store-opening-hours">' + openingHours + '</div>' +
                      '<div class="bottom-row">' +
                         '<div class="phone-number">' + phoneNumber + '</div>' +
                         '<div class="store-level">' + level + '</div>' +
                      '</div>' +
                   '</div>' +                       
            "</a>";

        return content;
    }

    //Test function.
    function GetStoreDetails(sapId, callback) {
        var myjson = '{ "SapId": "111", "StoreName": "Aldi", "StoreLogoUrl": "/~/media/shopping-centre/common/others/2013/11/aldi-logo-resized.ashx", "StorePageUrl": "/shopping-centres/centres/stockland-merrylands/stores/aldi", "OpeningHours": "8:30am - 9:00pm", "PhoneNumber": "13 25 34", "Level": "Ground" }';
        //myjson = "";
        callback(myjson);        
        return myjson;
    }

    function getStoreDetails(sapId, callback) {
        if (sapId != '') {
            $.ajax({
                url: "/public/map/GetRetailStoreDetails",
                method: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'storeSapId':" + JSON.stringify(Id) + "}",
                success: function (response) {
                    callback(JSON.stringify(response[0]));
                },
                error: function (exception) {
                    console.log('Could not retrieve Store details : ' + exception);
                    callback("");
                }
            });
        }
    }

});

//change the color of polygon graphics returned by a successful search. 
//this.map.infoWindow.set("fillSymbol", new SimpleFillSymbol().setColor(new esriColor([0,0,0,.4])));