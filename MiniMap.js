require([
  "esri/map",
  "esri/tasks/QueryTask",
  "esri/toolbars/edit",
  "esri/graphic",
  "esri/symbols/PictureMarkerSymbol",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/Color",
  "esri/tasks/query",
  "esri/InfoTemplate",
  "esri/geometry/Point",
  "esri/layers/GraphicsLayer",
  "dojo/domReady!"
], function (
  Map, QueryTask, Edit, Graphic,
  PictureMarkerSymbol, SimpleFillSymbol, SimpleLineSymbol,
  Color, Query, InfoTemplate,
  Point, GraphicsLayer
) {
    var map;
    
    //Constants----------------------
    esri.arcgis.utils.arcgisUrl = "http://stockland.maps.arcgis.com/sharing/rest/content/items";
    var mapid = "313e1aa7cf884ad1bfc54974186da3c7";
    var levelFieldName = "Level_";
    var sapIdFieldName = "SapID";
    var searchFiledName = "Tenant"; //This field is used in search widget as store name
    var storeLayerName = "StocklandRetailCentres_WFL1 - StocklandStores";
    //var StoreSapId = "20176-B0001-F000-000011";

    //Get the webmap item info. Access the store layer feature service to query the store geometry and also change the definition query to store level.
    var deferred = esri.arcgis.utils.getItem(mapid);
    deferred.then(function (response) {
        var storeLayerUrl;
        var layers = response.itemData.operationalLayers;
        for (var j = 0, jl = layers.length; j < jl; j++) {
            if (layers[j] != "labels" && layers[j].title == storeLayerName) {
                storeLayerUrl = layers[j].url;
            }
        }       
        createMapAnZoomToStore(storeLayerUrl, response);
    });


    function createMapAnZoomToStore(storeLayerUrl, itemInfo) {
        //First get the store extent
        var query = new Query();
        query.outFields = ['*'];
        query.returnGeometry = true; 
        query.where = sapIdFieldName + " = '" + StoreSapId + "'";

        var queryTask = new QueryTask(storeLayerUrl);
        queryTask.execute(query, function (featureset) {
            var store = featureset.features[0];
            var storeLevel = store.attributes[levelFieldName];
            var storeExtent = store.geometry.getExtent().expand(3)

            //Then change storelayer definition query to show just the selected store level.Do it by updating the webmap itemInfo.
            var layers = itemInfo.itemData.operationalLayers;
            for (var j = 0, jl = layers.length; j < jl; j++) {
                if (layers[j] != "labels" && layers[j].title == storeLayerName) {
                    layers[j].layerDefinition.definitionExpression = levelFieldName + " = '" + storeLevel + "'";                    
                    continue;
                }
            }

            //Create map using the updated itemInfo and setting the map extent to store Extent.
            var webmap = itemInfo;
            var deferred = esri.arcgis.utils.createMap(webmap, "map", {
                mapOptions: {
                    slider: false,
                    extent: storeExtent,
                    logo: false
                }
            });

            deferred.then(function (response) {
                map = response.map;

                map.infoWindow.set("anchor", "top");

                var storeLayerId;
                for (var j = 0, jl = map.graphicsLayerIds.length; j < jl; j++) {
                    if (map.graphicsLayerIds[j] != "labels") {
                        var currentLayer = map.getLayer(map.graphicsLayerIds[j]);
                        if (currentLayer.arcgisProps.title == storeLayerName) {
                            storeLayerId = map.getLayer(map.graphicsLayerIds[j]).id;
                            continue;
                        }
                    }
                }

                addHighlightingEffect(storeLayerId);
                map.disableScrollWheelZoom();

            }, function (error) {
                console.log("Error: ", error.code, " Message: ", error.message);
                deferred.cancel();
            });
        });
    }

  
    function addHighlightingEffect(storeLayerId) {
        var storeLayer = map.getLayer(storeLayerId);

        var highlightSymbol = new SimpleFillSymbol(
         SimpleFillSymbol.STYLE_SOLID,
         null,
         new Color([170, 190, 255, 0.35])
        );

        storeLayer.on("mouse-up", function (evt) {
            map.graphics.clear();

            var highlightGraphic = new Graphic(evt.graphic.geometry, highlightSymbol);
            map.graphics.add(highlightGraphic);

            if (!evt.graphic.attributes[searchFiledName] || evt.graphic.attributes[searchFiledName] == "")
                return;

            getStoreDetails(evt.graphic.attributes["SAPID"],
               function (response) {
                   if (response == "") //No responce from the callout
                       response = '{"StoreName": "' + evt.graphic.attributes[searchFiledName] + '" }';                  

                   var infoTemplate = new InfoTemplate({ anchor: "top" });
                   infoTemplate.setTitle(" ");
                   infoTemplate.setContent(createPopupContent(response));
                   evt.graphic.setInfoTemplate(infoTemplate);
               });
        });
    }

    function createPopupContent(response) {
        var storeInfoArr = JSON.parse(response);
        var content;
        if (Object.keys(storeInfoArr).length == 1) {
            //Showing just the store name
            var storename = storeInfoArr.StoreName;
            content = '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '</div>';
            return content;
        }

        var storename = storeInfoArr.StoreName;
        var storeUrl = storeInfoArr.StorePageUrl;
        var storeLogo = storeInfoArr.StoreLogoUrl;
        var openingHours = storeInfoArr.OpeningHours;
        var phoneNumber = storeInfoArr.PhoneNumber;
        var level = storeInfoArr.Level;
        var storePageUrl = storeInfoArr.StorePageUrl;

        //Create popup html content
        content = "<a  href =" + "'" + storePageUrl + "'" + ">" +
            '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '<div class="store-opening-hours">' + openingHours + '</div>' +
                      '<div class="bottom-row">' +
                         '<div class="phone-number">' + phoneNumber + '</div>' +
                         '<div class="store-level">' + level + '</div>' +
                      '</div>' +
                   '</div>' +
            "</a>";

        return content;
    }

    //Test function.
    function GetStoreDetails(sapId, callback) {
        var myjson = '{ "SapId": "111", "StoreName": "Aldi", "StoreLogoUrl": "/~/media/shopping-centre/common/others/2013/11/aldi-logo-resized.ashx", "StorePageUrl": "/shopping-centres/centres/stockland-merrylands/stores/aldi", "OpeningHours": "8:30am - 9:00pm", "PhoneNumber": "13 25 34", "Level": "Ground" }';
        myjson = "";
        callback(myjson);
        return myjson;
    }

    function getStoreDetails(sapId, callback) {
        if (sapId != '') {
            $.ajax({
                url: "/public/map/GetRetailStoreDetails",
                method: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'storeSapId':" + JSON.stringify(Id) + "}",
                success: function (response) {
                    callback(JSON.stringify(response[0]));
                },
                error: function (exception) {
                    console.log('Could not retrieve Store details : ' + exception);
                    callback("");
                }
            });
        }
    }

});
